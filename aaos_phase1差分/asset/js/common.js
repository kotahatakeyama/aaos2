$(document).ready(function(){
	// スマホメニュー起動
	$("#nav-toggle").on("click", function() {
		var gnavOpa = $('.gnav-wrap').css('display');
		if (gnavOpa === 'none') {
			$("#gnav_sp").css({right: '0%', opacity: '1'});
			$(".gnav-wrap").css({display: 'block'});
			$(".gnav-wrap").animate({opacity: '1'}, {duration: 500});
  		$(this).addClass("active");
		}
		else {
			$("#gnav_sp").css({right: '-100%', opacity: '0'});
			$(".gnav-wrap").animate({opacity: '0'}, {duration: 500});
      $(".gnav-wrap").css({display: 'none'});
  		$(this).removeClass("active");
		}
	});
	//スマホメニュー任意のクリックで閉じる
	$("#gnav_sp").click(function() {
    $("#gnav_sp").css({right: '-100%', opacity: '0'});
    $(".gnav-wrap").animate({opacity: '0'}, {duration: 500});
    $(".gnav-wrap").css({display: 'none'});
		$("#nav-toggle").removeClass("active");
	});
});

// スクロールバー色変更
$(function(){
	$('.topics dl').jScrollPane({
		arrowScrollOnHover: true
	});
});

// チェックボックスをクリックしたらボタン表示 (2箇所チェック対応)
$(function() {
	$('.select-box li').on('click', function(){
		var index = $('.select-box li').index(this);
		console.log(index, 'index');
		$('input[name=select]:eq('+index+')').prop('checked', true);

		if($("[name=select2]").size() > 0) {
			temp1 = $("[name=select]:checked").val();
			temp2 = $("[name=select2]:checked").val();
			if ((typeof temp1 != "undefined") && (typeof temp2 != "undefined")) {
				$("form button").animate({opacity: 1}, {duration:500});
				$("form button").css({display: 'block'});
			}
		}
		else {
			$("form button").animate({opacity: 1}, {duration:500});
			$("form button").css({display: 'block'});
		}
	});
});

// チェックボックスのチェック位置を記録する
function set() {
	var select_num = $("[name=select]:checked").val();
	sessionStorage.setItem('select_num', select_num);
	var select_num2 = $("[name=select2]:checked").val();
	sessionStorage.setItem('select_num2', select_num2);
}

// Self-Assessment Examination 説明モーダル起動
$(document).ready(function(){
	$(".about").on("click", function() {
		$(".modal").animate({opacity: 1, zIndex: 8}, {duration:300});
		$(".modal-wrap").css({display: 'block', zIndex: -1});
	});
	$(".close-wrap").on("click", function() {
		$(".modal").css({opacity: 0, zIndex: -10});
		$(".modal-wrap").css({display: 'none'});
	});
	$(document).click(function(event){
		var $target = $(event.target);
		if($target.is(".modal-inner") || $target.is(".modal-inner h2") || $target.is(".modal-inner p")){
		}
		else {
			$(".modal").css({opacity: 0, zIndex: -10});
			$(".modal-wrap").css({display: 'none', zIndex: -10});
		}
	});
});

// 写真ズームモーダル起動
$(function() {
	// モーダル起動
	$('.pictures > li').click(function(){
    if ($('.picture-wrap').css('display') === "none" ) {
  		$openModal = "." + $(this).attr("class") + "-modal";
  		$($openModal).animate({opacity: 1, zIndex: 8}, {duration:300});
  		$(".picture-wrap").animate({opacity: 1}, {duration:300});
  		$(".picture-wrap").css({display: 'block', zIndex: 7});
  		$(".picture-wrap").wrap('<div class="modal-bg" onclick=""></div>');
      console.log($('.pictures-modal > li').length);
      if ($('.pictures-modal > li').length < 2) {
        $('.right-arrow').css({display: 'none'});
        $('.left-arrow').css({display: 'none'});
      }
    }
	});
	// 右矢印押下
	$(".right-arrow").on("click", function() {
		for (var i = 1; i <= $('.pictures > li').length; i++) {
			$openModal = ".picture" + i + "-modal";
			if($($openModal).css('opacity') == "1" ){
				$str = $openModal.split('-modal');
				$str2 = $str[0].split('.picture');
				$next = parseInt($str2[1], 10);
				$next = $next + 1;
				if ($next > $('.pictures > li').length) {
					$next = 1;
				}
				$nextOpenModal = ".picture" + $next + "-modal";
//				$($openModal).animate({opacity: 0}, {duration:1000});
				$($openModal).animate({opacity: 0});
				$($openModal).css({zIndex: -10});
				$($nextOpenModal).css({zIndex: 8});
//				$($nextOpenModal).animate({opacity: 1}, {duration:1000});
				$($nextOpenModal).animate({opacity: 1});
			}
		}
	});
	// 左矢印押下
	$(".left-arrow").on("click", function() {
		for (var i = 1; i <= $('.pictures > li').length; i++) {
			$openModal = ".picture" + i + "-modal";
			if($($openModal).css('opacity') == "1" ){
				$str = $openModal.split('-modal');
				$str2 = $str[0].split('.picture');
				$prev = parseInt($str2[1], 10);
				$prev = $prev - 1;
				if ($prev === 0) {
					$prev = $('.pictures > li').length;
				}
				$prevOpenModal = ".picture" + $prev + "-modal";
//				$($openModal).animate({opacity: 0}, {duration:1000});
				$($openModal).animate({opacity: 0});
				$($openModal).css({zIndex: -10});
				$($prevOpenModal).css({zIndex: 8});
//				$($prevOpenModal).animate({opacity: 1}, {duration:1000});
				$($prevOpenModal).animate({opacity: 1});
			}
		}
	});

	// スワイプにて画像入れ替え対応
	for (var i = 1; i <= $('.pictures > li').length; i++) {
		$openModal = ".picture" + i + "-modal";
		$($openModal).on("touchstart" , TouchStart);
		$($openModal).on("touchmove" , TouchMove);
		$($openModal).on("touchend" , TouchEnd);
	}

	// タップした位置をメモリーする
	function TouchStart( event ) {
		move = "initialized";
		event.preventDefault();                     // ページが動いたり、反応を止める（A タグなど）
		var touch = event.originalEvent.touches[0];
    var $target = $(document.elementFromPoint(touch.clientX, touch.clientY));
		var $pos = touch.clientX; //Xを得る

		for (var i = 1; i <= $('.pictures > li').length; i++) {
			$openModal = ".picture" + i + "-modal";
			if ($($openModal).css('opacity') == "1" ){
				$currentOpenModal = ".picture" + i + "-modal";
			}
		}
		$($currentOpenModal).data("memory", $pos);
	}

	// 動いた方向を検知する
	function TouchMove(event) {
		event.preventDefault();                     // ページが動くのを止める
		var touch = event.originalEvent.touches[0];
    var $target = $(document.elementFromPoint(touch.clientX, touch.clientY));
		var $pos = touch.clientX; //Xを得る

		for (var i = 1; i <= $('.pictures > li').length; i++) {
			$openModal = ".picture" + i + "-modal";
			if ($($openModal).css('opacity') == "1" ){
				$currentOpenModal = ".picture" + i + "-modal";
			}
		}
		// 左右判定
		if( $pos < ($target.data("memory") - 1.5) ){
			move = "left";
		} else if ( $pos > ($target.data("memory") + 1.5) ){
			move = "right";
		} else {
			move = "initialized";
		}
	}

	// ゆびを話した時点で画像を入れ替える
	function TouchEnd(event) {
		// if(move === "left"){
		if(move === "right"){
			$str = $currentOpenModal.split('-modal');
			$str2 = $str[0].split('.picture');
			$prev = parseInt($str2[1], 10);
			$prev = $prev - 1;
			if ($prev === 0) {
				$prev = $('.pictures > li').length;
			}
			$prevOpenModal = ".picture" + $prev + "-modal";
//			$($currentOpenModal).animate({opacity: 0}, {duration:500});
			$($currentOpenModal).animate({opacity: 0});
			$($currentOpenModal).css({zIndex: -10});
			$($prevOpenModal).css({zIndex: 8});
//			$($prevOpenModal).animate({opacity: 1}, {duration:500});
			$($prevOpenModal).animate({opacity: 1});
		// } else if (move === "right"){
		} else if (move === "left"){
			$str = $currentOpenModal.split('-modal');
			$str2 = $str[0].split('.picture');
			$next = parseInt($str2[1], 10);
			$next = $next + 1;
			if ($next > $('.pictures > li').length) {
				$next = 1;
			}
			$nextOpenModal = ".picture" + $next + "-modal";
//			$($currentOpenModal).animate({opacity: 0}, {duration:500});
			$($currentOpenModal).animate({opacity: 0});
			$($currentOpenModal).css({zIndex: -10});
			$($nextOpenModal).css({zIndex: 8});
//			$($nextOpenModal).animate({opacity: 1}, {duration:500});
			$($nextOpenModal).animate({opacity: 1});
		}
	}

	// CLOSE押下
	$(".close-wrap").on("click", function() {
		for (var i = 1; i <= $('.pictures > li').length; i++) {
			$openModal = ".picture" + i + "-modal";
			if($($openModal).css('opacity') == "1" ){
				$($openModal).css({opacity: 0, zIndex: -10});
			}
		}
		$(".picture-wrap").css({display: 'none', zIndex: -10, opacity: 0}).unwrap();
	});
	// 画像以外押下でもCLOSE
	$(document).on('click','.modal-bg',function(event){
		var $pictWrap = $('.picture-wrap').css('display');
		if ($pictWrap === "block") {
			var $target = $(event.target);
			if($target.is(".pictures > li") || $target.is(".pictures-modal > li") || $target.is("img")) {
			}
			else {
				for (var i = 1; i <= $('.pictures > li').length; i++) {
					$openModal = ".picture" + i + "-modal";
					if($($openModal).css('opacity') == "1" ){
						$($openModal).css({opacity: 0, zIndex: -10});
					}
				}
				$(".picture-wrap").css({display: 'none', zIndex: -10, opacity: 0}).unwrap();
			}
		}
	});
});

// Gナビの固定表示部分一時解除
$(document).ready(function(){
	var img_dir = "http://" + window.location.hostname + "/asset/image/";
	$("nav ul li").hover(function() {
		$(".current span").css({opacity: "0"});
		$("ul li.current:after").css({opacity: "0"});
	},function(){
		$(".current span").css({opacity: "1"});
		$("ul li.current:after").css({opacity: "1"});
	});
});

// ワンポイントアドバイスポップアップ
$(document).ready(function(){
	$(".one-point").on("click", function() {
		$(".popup").animate({opacity: 1}, {duration:100});
		$(".popup").css({display: 'block', zIndex: '2'});
		$(".popup-inner").animate({opacity: 1}, {duration:100});
		$(".popup-inner").css({display: 'block'});
		$(".one-point").css({zIndex: '2'});
	});
	$(".close-wrap").on("click", function() {
		$(".popup").css({opacity: 0});
		$(".popup").css({display: 'none'});
		$(".one-point").css({zIndex: '1'});
	});
	$(document).click(function(event){
		var $target = $(event.target);
		if($target.is(".one-point") || $target.is(".popup-inner") || $target.is(".popup-inner h3") || $target.is(".popup-inner p") || $target.is("img")){
		}
		else {
			$(".popup").css({opacity: 0});
			$(".popup").css({display: 'none'});
			$(".one-point").css({zIndex: '1'});
		}
	});
});


// スマホとタブレットでviewportを切替え
/*
$(function(){
    var ua = navigator.userAgent;
    if((ua.indexOf('iPhone') > 0) || ua.indexOf('iPod') > 0 || (ua.indexOf('Android') > 0 && ua.indexOf('Mobile') > 0)){
			$('head').prepend('<meta name="viewport" content="width=1023">');
    } else {
			$('head').prepend('<meta name="viewport" content="width=device-width,initial-scale=1">');
    }
});
*/

// スマホ(640px以下)で縮小表示
function reSize() {
  var portraitWidth,
		  landscapeWidth,
		  notElement = "p,address,span,h1,h2,h3,h4,h5,h6,dt,dd,dl";
	var w = $(window).width();
  target = $("html");
	// 640以下でのみ動作
	if (w <= 640) {
	  $(window).bind("resize", function() {
	    if (Math.abs(window.orientation) === 0) {
	      if (/Android/.test(window.navigator.userAgent)) {
	        if (!portraitWidth) {
	          portraitWidth = $(window).width();
	        }
	      } else {
	        portraitWidth = $(window).width();
	      }
	      target.css("zoom", portraitWidth / 640);
	    } else {
	      if (/Android/.test(window.navigator.userAgent)) {
	        if (!landscapeWidth) {
	          landscapeWidth = $(window).width();
	        }
	      } else {
	        landscapeWidth = $(window).width();
	      }
	      target.css("zoom", landscapeWidth / 640);
	    }
	  }).trigger("resize");
	}
	else if (w <= 768) {
		$(window).bind("resize", function() {
			if (Math.abs(window.orientation) === 0) {
				if (/Android/.test(window.navigator.userAgent)) {
					if (!portraitWidth) {
						portraitWidth = $(window).width();
					}
				} else {
					portraitWidth = $(window).width();
				}
				target.css("zoom", portraitWidth / 768);
			} else {
				if (/Android/.test(window.navigator.userAgent)) {
					if (!landscapeWidth) {
						landscapeWidth = $(window).width();
					}
				} else {
					landscapeWidth = $(window).width();
				}
				target.css("zoom", landscapeWidth / 768);
			}
		}).trigger("resize");
	}
	else if (w <= 1024) {
		$(window).bind("resize", function() {
			if (Math.abs(window.orientation) === 0) {
				if (/Android/.test(window.navigator.userAgent)) {
					if (!portraitWidth) {
						portraitWidth = $(window).width();
					}
				} else {
					portraitWidth = $(window).width();
				}
				target.css("zoom", portraitWidth / 1024);
			} else {
				if (/Android/.test(window.navigator.userAgent)) {
					if (!landscapeWidth) {
						landscapeWidth = $(window).width();
					}
				} else {
					landscapeWidth = $(window).width();
				}
				target.css("zoom", landscapeWidth / 1024);
			}
		}).trigger("resize");
	}
	else {
		target.css("zoom", 1);
	}
}
reSize();

// タブレット等で縦横が変更されたときのみリロード
$(document).ready(function(){
	if ('object' === typeof window.onorientationchange) {
	  window.addEventListener("orientationchange", function () {
	    if (window.innerHeight > window.innerWidth) {
	      location.reload();
	    } else {
	      location.reload();
	    }
	  }, false);
	}
});

// 画面リサイズ時に読み込み直す処理
$(document).ready(function(){
	var ua = navigator.userAgent;
    if (ua.indexOf('iPhone') > 0 || ua.indexOf('iPod') > 0 || ua.indexOf('Android') > 0 && ua.indexOf('Mobile') > 0) {
        // スマートフォン用コード
    } else if (ua.indexOf('iPad') > 0 || ua.indexOf('Android') > 0) {
        // タブレット用コード
    } else {
      // PC用コード
			var timer = false;
			$(window).resize(function() {
				if (timer !== false) {
					clearTimeout(timer);
				}
				timer = setTimeout(function() {
					//リロードする
					location.reload();
				}, 200);
			});
    }
});

// ブラウザ毎で個別のcssを読み込む
$(function() {
	if (window.location.hostname === "tohokuitfactory.com"){
		home_dir = "http://" + window.location.hostname + "/___coding___/aaos/";
	} else if (window.location.hostname === "www.loxonin.info"){
		home_dir = "http://" + window.location.hostname + "/aaos/test/test1/";
	} else if (window.location.hostname === "bmy-web.sakura.ne.jp") {
		home_dir = "http://" + window.location.hostname + "/AAOS/aaos/";
	} else if (window.location.hostname === "www.loxonin.info") {
		home_dir = "http://" + window.location.hostname + "/aaos/";
	} else if (window.location.hostname === "localhost") {
		home_dir = "http://" + window.location.hostname + "/aaos/phase3/";
	} else {
		home_dir = "http://" + window.location.hostname + "/";
	}

	if ((navigator.userAgent.indexOf('iPhone') > 0 && navigator.userAgent.indexOf('iPad') == -1) || navigator.userAgent.indexOf('iPod') > 0) {
		// iPhone, iPad用
	} else if (navigator.userAgent.indexOf('Android') > 0) {
//		document.write('<link rel="stylesheet" type="text/css" href="/css/android.css">');
			$('head').prepend('<link rel="stylesheet" type="text/css" href="' + home_dir + 'asset/css/android.css">');
  } else {
		// PC用
	}
});

// android で固定ヘッダが移動してしまう問題の対処
$(function(){
	if (navigator.userAgent.indexOf('Android') > 0) {
		$(".top-header").css({top: '0'});
	}
});

$(function () {
    if(navigator.userAgent.indexOf('Android') > 0){
        $("html").addClass("android");
    }
});
